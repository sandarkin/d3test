package ru.sandarkin.challenge.d3test;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.util.*;

/**
 * @author DHARMA Initiative
 */
@Controller
@RequestMapping("/")
public class MainController {

  private Random rnd = new Random();

  @RequestMapping(value = "/", method = RequestMethod.GET)
  public ModelAndView getHome() throws IOException {
    return new ModelAndView("home");
  }

  @RequestMapping(value = "/data/counts", method = RequestMethod.GET)
  @ResponseBody
  public List getCounts() {
    return generateCounts();
  }

  @RequestMapping(value = "/data/ages/{total}", method = RequestMethod.GET)
  @ResponseBody
  public List getAges(@PathVariable Integer total) {
    return generateAges(total);
  }

  private List<Map<String,Object>> generateAges(Integer total) {
    List<Map<String,Object>> list = new ArrayList<Map<String, Object>>();
    int[] agesCount = randSum(5,total);
    list.add(makeAges("18-25",agesCount[0]));
    list.add(makeAges("25-30",agesCount[1]));
    list.add(makeAges("30-40",agesCount[2]));
    list.add(makeAges("40-50",agesCount[3]));
    list.add(makeAges("50+",agesCount[4]));

    return list;
  }

  private Map<String, Object> makeAges(String age, int pop) {
    Map<String,Object> map = new HashMap<String, Object>();
    map.put("age", age);
    map.put("population", pop);
    return map;
  }


  private List generateCounts() {
    List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
    list.add(generateCount("Альфа"));
    list.add(generateCount("Ренессанс"));
    list.add(generateCount("Хоум"));
    list.add(generateCount("Сбер"));
    list.add(generateCount("Открытие"));
    list.add(generateCount("ВТБ24"));
    return list;
  }

  private Map<String, Object> generateCount(String bank) {
    Map<String, Object> map = new HashMap<String,Object>();
    map.put("bank",bank);
    map.put("to10",randomInt(20,70));
    map.put("to50",randomInt(10,40));
    map.put("to100",randomInt(10,30));
    map.put("over100",randomInt(5,20));
    return map;
  }

  private int randomInt(int min, int max) {
    return rnd.nextInt(max - min) + min;
  }

  private static int[] randSum(int count, int total) {
    int[] nums = new int[count];
    int upperbound = Long.valueOf(Math.round(total*1.0/count)).intValue();
    int offset = Long.valueOf(Math.round(0.5*upperbound)).intValue();

    int cursum = 0;
    Random random = new Random(new Random().nextInt());
    for(int i=0 ; i < count ; i++){
      int rand = random.nextInt(upperbound) + offset;
      if( cursum + rand > total || i == count - 1) {
        rand = total - cursum;
      }
      cursum += rand;
      nums[i]=rand;
      if(cursum == total){
        break;
      }
    }
    return nums;
  }



}
