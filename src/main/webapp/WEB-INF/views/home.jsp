<%@ page pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>D3 Demo</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="assets/style.css">
    <script src="//code.jquery.com/jquery-1.10.1.min.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.2/js/bootstrap.min.js"></script>
    <script src="//d3js.org/d3.v3.min.js"></script>
</head>
<body>

<div id="wrap">

    <div class="container">
        <div class="page-header">
            <h1>D3 Demo</h1>
        </div>
        <p class="lead small">Столбиковый с разбивкой на составляющие график, показывает, количество выданных кредитов в разрезе банков.
            Каждый такой столбик складывается из столбиков, обозначающих количество выданных кредитов на опеределлый диапазон суммы.<br/>
            Круговая диаграмма показывает разбивку клиентов банка, получивших кредит, по диапазону их возраста<br/>
            Для того, чтобы посмотреть разбивку по клиентам какого-либо банка, нужно кликнуть на стобце, ему соответствующем.</p>
        <div id="graph"></div>
        <div id="donut"></div>

        <script type="text/javascript" src="assets/app.js"></script>
    </div>
</div>

<div id="footer">
    <div class="container">
        <p class="text-muted credit">Различные пояснения</p>
    </div>
</div>

</body>
</html>