d3.json("data/counts", function(error, data) {
  plotChart(data);
});

var margin = {top: 20, right: 20, bottom: 30, left: 40},
  width = 400 - margin.left - margin.right,
  height = 300 - margin.top - margin.bottom;

var x = d3.scale.ordinal()
  .rangeRoundBands([0, width], .1);

var y = d3.scale.linear()
  .rangeRound([height, 0]);

var color = d3.scale.ordinal()
  .range(["#98abc5", "#7b6888", "#a05d56", "#ff8c00"]);

var xAxis = d3.svg.axis()
  .scale(x)
  .orient("bottom");

var yAxis = d3.svg.axis()
  .scale(y)
  .orient("left")
  .tickFormat(d3.format(".2s"));

var svg = d3.select("#graph").append("svg")
  .attr("width", width + margin.left + margin.right)
  .attr("height", height + margin.top + margin.bottom)
  .append("g")
  .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

var donutColor = d3.scale.ordinal().range(["#98abc5", "#8a89a6", "#7b6888", "#6b486b", "#a05d56", "#d0743c", "#ff8c00"]);
var donutWidth = 400, donutHeight = 300, donutRadius = Math.min(donutWidth, donutHeight) / 2;
var pie = d3.layout.pie().value(function(d) { return d.population; }).sort(null);
var arc = d3.svg.arc().outerRadius(donutRadius - 10).innerRadius(donutRadius - 70);
var donutArc;
var donutSvg = d3.select("#donut").append("svg")
  .attr("width", donutWidth)
  .attr("height", donutHeight)
  .append("g")
  .attr("transform", "translate(" + donutWidth / 2 + "," + donutHeight / 2 + ")");

function plotChart(data) {
  color.domain(["to10","to50","to100","over100"]);

  data.forEach(function(d) {
    var y0 = 0;
    d.counts = color.domain().map(function(name) {return {name: name, y0: y0, y1: y0 += +d[name]}; });
    d.total = d.to10 + d.to50 + d.to100 + d.over100;
  });
  data.sort(function(a, b) { return b.total - a.total; });

  x.domain(data.map(function(d) { return d.bank; }));
  y.domain([0, d3.max(data, function(d) { return d.total; })]);

  svg.append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(0," + height + ")")
    .call(xAxis);

  svg.append("g")
    .attr("class", "y axis")
    .call(yAxis)
    .append("text")
    .attr("transform", "rotate(-90)")
    .attr("y", 2)
    .attr("dy", ".5em")
    .style("text-anchor", "end")
    .text("Количество кредитов");

  var state = svg.selectAll(".state")
    .data(data)
    .enter().append("g")
//    .on("click", function(drect) {showAges(drect);})
    .attr("class", "g clickable")
    .attr("transform", function(d) { return "translate(" + x(d.bank) + ",0)"; });

  state.selectAll("rect")
    .data(function(d) { return d.counts; })
    .enter().append("rect")
    .attr("width", x.rangeBand())
    .attr("y", function(d) { return y(d.y1); })
    .attr("height", function(d) { return y(d.y0) - y(d.y1); })
    .attr("class", "click-capture")
    .style("fill", function(d) { return color(d.name); });

  var legend = svg.selectAll(".legend")
    .data(color.domain().slice().reverse())
    .enter().append("g")
    .attr("class", "legend")
    .attr("transform", function(d, i) { return "translate(0," + i * 20 + ")"; });

  legend.append("rect")
    .attr("x", width - 18)
    .attr("width", 18)
    .attr("height", 18)
    .style("fill", color);

  legend.append("text")
    .attr("x", width - 24)
    .attr("y", 9)
    .attr("dy", ".35em")
    .style("text-anchor", "end")
    .text(function(d) {
      switch (d) {
        case "to10":
          return "до 10000 руб.";
        case "to50":
          return "до 50000 руб.";
        case "to100":
          return "до 100000 руб.";
        case "over100":
          return "более 100000 руб.";
      }
      return d;
    });
}


d3.json("data/ages/"+100, function(error, data) {
  var path = donutSvg.datum(data).selectAll("path")
    .data(pie)
    .enter().append("path")
    .attr("fill", function(d, i) { return donutColor(d.data.age); })
    .attr("d", arc);


  var g = donutSvg.selectAll(".arc")
    .data(pie(data))
    .enter().append("g")
    .attr("class", "arc");

  g.append("text")
    .attr("transform", function(d) { return "translate(" + arc.centroid(d) + ")"; })
    .attr("dy", ".35em")
    .style("text-anchor", "middle")
    .text(function(d) { return d.data.age; });
  donutArc = g;

  d3.selectAll(".clickable").on("click", function(drect) {change(drect);});
  function change(obj) {
    d3.json("data/ages/"+obj.total, function(e, newData) {
      donutSvg.datum(newData);
      pie.value(function(d) { return d.population; }).sort(null);

      donutArc.selectAll("text").remove();
      donutArc.data(pie(newData));
      donutArc.append("text")
        .attr("transform", function(d) { return "translate(" + arc.centroid(d) + ")"; })
        .attr("dy", ".35em")
        .style("text-anchor", "middle")
        .text(function(d) { return d.data.age; });

      path = path.data(pie); // compute the new angles
      path.attr("d", arc); // redraw the arcs


    });
  }
});
